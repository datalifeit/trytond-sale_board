datalife_sale_board
===================

The sale_board module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_board/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_board)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
